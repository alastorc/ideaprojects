package chapter.one;

/**
 * Created by alastorc on 1/21/2016.
 */
public class problem1 {
    /*
    * My initial solution. Works well, but is not optimal.
    * Runs in O(n^2) time
    * Does not use any additional data structure
    * NOTE: I did not think to verify whether the characters
    * were encoded with Unicode or ASCII.
    * --> ASCII only has 256 chars, any String >256 infers dups
    * */
    public boolean checkStrDups(String s) {
        int i, j;
        //TRUE means dups, FALSE means none
        if (s.length() > 256)
            return true;

        for (i = 0; i < s.length()-1; i++) {
            for (j = i+1; j < s.length(); j++) {
                System.out.println(s.charAt(i) + " " + s.charAt(j));
                if (s.charAt(i) == s.charAt(j)) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
    * Primary Solution in book
    * Optimal run time of O(n)
    * However, it does use an additional data structure
    */
    public boolean checkStrDups2(String s) {
        //TRUE means dups, FALSE means none
        if (s.length() > 256)
            return true;

        boolean[] char_set = new boolean[256];
        for (int i = 0; i < s.length(); i++) {
            int val = s.charAt(i);
            System.out.println(val);
            if (char_set[val]) {  //already found this char in string
                return true;
            }
            char_set[val] = true;
        }
        return false;
    }
}
