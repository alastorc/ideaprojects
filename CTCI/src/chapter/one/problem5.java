package chapter.one;

/**
 * Created by alastorc on 1/28/2016.
 */
public class problem5 {
    public char[] compressStr(char[] orig, int len) {

        //checking to see if compression would be longer than original string
        int newSize = checkSize(orig, len);
        //TEST
        System.out.println("new size is: " + newSize);
        if (newSize >= len) {
            System.out.println("could not compress");
            return orig;
        }

        /* -------------------------------------
         * The value len is the maximum needed size for the array below that
         * contains the number of repeats for each char.
         * If the the orig array did not have a single repeating character this
         * array would still be big enough to hold all the values, as it only contains
         * the # of repeats and not the characters themselves
        ---------------------------------------- */
        int numReps[] = new int[len];
        int intIndex = 0;  //so we know where we are in the numReps array

        char prev = orig[0];  //getting ready for comparisons
        int count = 1;  //for counting # of repeats for each character

        for (int i = 1; i < len; i++) {
            if (orig[i] == prev) {
                count++;
            } else {
                System.out.println("count for " + prev + ": " + count);
                numReps[intIndex] = count;
                intIndex++;
                count = 1;
                prev = orig[i];
            }
        }
        System.out.println("count for " + prev + ": " + count);
        numReps[intIndex] = count;

        //TEST
        System.out.println("array of counts");
        int z = 0;
        while (numReps[z] != 0 && z < len) {
            System.out.print(numReps[z] + " ");
            z++;
        }
        System.out.println();

        char[] newStr = new char[newSize];
        int i = 0, j = 0, k = 0;
        //TODO seems that when the last char only occurs once it does not get printed. Why?
        while (i < newSize) {
            System.out.println("char being added: " + orig[j]);
            newStr[i] = orig[j];
            i++;
            //parsing char value of count value and adding to the new char array
            char[] chrCnt = String.valueOf(numReps[k]).toCharArray();
            for (char c : chrCnt) {
                newStr[i] = c;
                i++;
            }
            j = j + numReps[k];  //moving through old string by # of repeats for each char
            k++;  //moving through array numReps
            System.out.println("i: " + i + " j: " + j + " k: " + k);
        }
        System.out.println("string has been compressed");
        return newStr;
    }


    /* Method to check if the new string will be shorter than the original */
    public int checkSize(char[] s, int len) {
        //checking to see if there are any characters in the array
        if (s == null)
            return 0;

        int count = 1, size = 0;
        char current = s[0]; //ready for first comparison

        for (int i = 0; i < len; i++) {
            if (s[i] == current) {
                count++;
            } else {
                current = s[i];
                count = 1;
                //this method of finding integer length was in the book. cut & pasted basically
                size += 1 + String.valueOf(count).length(); //determining length of the integer value
            }
        }
        size += 1 + String.valueOf(count).length(); //determining length of the integer value
        return size;
    }
}
