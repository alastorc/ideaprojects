package chapter.one;

/**
 * Created by alastorc on 1/26/2016.
 *
 * Problem:
 * Given two strings, write a method to decide if one is a permutation of the other.
 */
public class problem3 {
    public boolean checkPermute(String str1, String str2) {
        //first trial. if not equal length no need to try anything else.
        if (str1.length() != str2.length()) {
            return false;
        }

        /*
         * If strings have same number of same characters then
         * they are permutations of each other.
         */

        //break strings into character arrays
        char[] s_arr1 = str1.toCharArray();
        //create array to hold # of character occurrences
        int[] charOccur = new int[256];  //assuming ASCII encoding

        int count = 0;
        //for-each loop to count occurrences in first string
        for (char c : s_arr1) {
            //System.out.println(count + " " + c);
            charOccur[c]++;  //not (int) cast needed, char treated as an int
            count++;
        }

        // TEST
        for (int i = 0; i < charOccur.length; i++) {
            if (charOccur[i] > 0)
                System.out.print(i + ":" + charOccur[i] + ", ");
        }
        System.out.println();
        // END TEST

        /* Going to do this a bit different than the book solution and simply
         * use a for-each to loop through the second string like the first.
         * Subtract from the counts, and when any count gets below 0 return false, else true.
         */
         char[] s_arr2 = str2.toCharArray();
         for (char c : s_arr2) {
             charOccur[c]--;
             if (charOccur[c] < 0) {
                 System.out.println("Uneven char count.  Culprit: " + c);
                 return false;
             }
         }
        return true;
    }
}
