package chapter.one;

/**
 * Created by alastorc on 1/29/2016.
 *
 * Rotate a matrix 90 degrees
 * My solution does it in place, without a superfluous array for storage
 */

public class problem6 {
    public int[][] rotate90(int[][] matrix, int N) {
        int currVal, nextVal;
        int row, col, nextRow, nextCol;

        for (int layer = 0; layer < N/2; layer++) {  //going through each layer, from outer to inner
            row = layer;  //first row for the beginning of the rotation

            //TEST
            for (int i = layer; i < (N - (layer + 1)); i++) {  //each value in the top row of each layer
                col = i;  //first column for the beginning of the rotation
                currVal = matrix[row][col];

                //TEST
                System.out.println("initial row: " + row + " initial col: " + col);
                System.out.println("next value in top row of layer: " + currVal);

                for (int dim = 1; dim <= 4; dim++) {  //matrix has 4 sides
                    nextRow = col;
                    nextCol = N - 1 - row;
                    nextVal = matrix[nextRow][nextCol];

                    //TEST
                    System.out.println("next row: " + nextRow + " next col: " + nextCol);
                    System.out.println("next value: " + nextVal);
                    matrix[nextRow][nextCol] = currVal;
                    currVal = nextVal;
                    row = nextRow;
                    col = nextCol;
                    System.out.println();
                }
            }
        }

        return matrix;
    }
}
