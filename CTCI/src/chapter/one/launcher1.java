package chapter.one;

/**
 * Created by alastor on 1/20/2016.
 */
public class launcher1 {

    public static void main(String[] args) {

        /* PROBLEM #1 ---------------------
        String myString = new String();
        myString = "duplicatess!";
        problem1 p1 = new problem1();
        boolean test1;
        test = p1.checkStringDups(myString);
        System.out.println(test1);
        --------------------------------- */

        /*--- PROBLEM #2 is written in C. Check VM ---*/

        /* PROBLEM #3 --------------------
        String s1 = "yessir";
        String s2 = "yessit";
        problem3 p3 = new problem3();
        boolean test3;
        test3 = p3.checkPermute(s1, s2);
        System.out.println(test3);
        -----------------------------------*/

        /*--- PROBLEM #4 is written in C. Check VM ---*/

        /* PROBLEM #5 -----------------------
        String hue = "aaaaaaaaaaaaaaaaaaaaaaazzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzbcd";
        int length = hue.length();
        char charArr[] = hue.toCharArray();
        System.out.println(charArr);

        problem5 p5 = new problem5();
        char[] r = p5.compressStr(charArr, length);
        System.out.println(r);
        ----------------------------------- */

        /* --- PROBLEM #6 -------------------
        int[][] initMat = {{10,20,30,40}, {50,60,70,80}, {90, 100, 110, 120}, {130,140,150,160}};
        int dim = 4;

        //PRINT
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                System.out.print(initMat[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        problem6 p6 = new problem6();
        int[][] r = p6.rotate90(initMat, dim);

        //PRINT RETURN
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                System.out.print(r[i][j] + " ");
            }
            System.out.println();
        }
        ------------------------------------- */

        /* PROBLEM #7 ------------------------- */
        int m = 4;
        int n = 7;

        int[][] newMat = {{6, 17, 8, 0, 2, 50, 78}, {0, 67, 5, 12, 6, 7, 88}, {45, 7, 16, 28, 70, 6, 7}, {32, 33, 72, 9, 4, 8, 0}};

        //PRINT
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(newMat[i][j] + " ");
            }
            System.out.println();
        }

        problem7 p7 = new problem7();
        newMat = p7.clearRowCol(newMat, m, n);

        //PRINT
        System.out.println();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(newMat[i][j] + " ");
            }
            System.out.println();
        }
    }
}
