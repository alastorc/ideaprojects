package chapter.one;

/**
 * Created by alastor on 1/20/2016.
 *
 * Problem:
 * Implement an algorithm to determine if a string has all unique characters. What if
 * you cannot use additional data structures?
 *
 */

public class problem1 {

    public boolean checkStringDups(String s) {
        int i, j;
        /*
        * iterate through the string from the first character to the second to last
        * and compare the character at any i to all the characters following it
        * up to the last character in the string.
        * If a duplicate is found, return true
        * Otherwise, the outer for will loop until the last character
        * and then return false.  No need to check past the second to last character
        * as there is nothing to compare it to.
        */
        for (i = 0; i < s.length()-1; i++) {
            for (j = i+1; j < s.length(); j++) {
                System.out.println(s.charAt(i) + " " + s.charAt(j));
                if (s.charAt(i) == s.charAt(j)) {
                    return true;
                }

            }
        }
        return false;
    }
}
