package chapter.one;

/**
 * Created by alastorc on 1/31/2016.
 *
 * Problem:
 * Write an algorithm such that if an element in an M x N matris is 0, its entire row and column are set to 0
 *
 * Solution:
 * Create boolean arrays, single dimension, and then loop through the entire matrix, storing the locations of
 * zeros at the corresponding indices of arrays.  Go back through the single dimension arrays and zero out
 * appropriate rows & columns of the matrix.
 * This avoids using an array of size M x N additional to the original array, and going back through at the end
 * to zero it all out prevents the issue of everything becoming a 0.
 */
public class problem7 {
    public int[][] clearRowCol(int[][] matrix, int M, int N) {
        //using these avoids having the M x N 2D array
        boolean[] isRowZero = new boolean[M];
        boolean[] isColZero = new boolean[N];

        //Loop through entire matrix, store locations of 0s in arrays
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (matrix[i][j] == 0) {
                    isRowZero[i] = true;
                    isColZero[j] = true;
                }
            }
        }

        //time to clear the rows and columns
        //oculd make this more concise, but both loops together
        for (int i = 0; i < M; i++) {
            if (isRowZero[i]) {
                for (int j = 0; j < N; j++) {  //clear out the row
                    matrix[i][j] = 0;
                }
            }
        }
        for (int j = 0; j < N; j++) {
            if (isColZero[j]) {
                for (int i = 0; i < M; i++) {  //clear out the column
                    matrix[i][j] = 0;
                }
            }
        }

        return matrix;
    }
}
