package chapter.two;

/**
 * Created by alastorc on 2/3/2016.
 *
 * Problem:
 *   You have 2 numbers represented by a linked list, where each node contains a single digit.
 *   The digits are stored in reverse order, such that the 1's digit is @ the head of the list.
 *   Write a function that adds the two numbers & returns the sum as a linked list.
 *   FOLLOW UP:
 *   Suppose the digits are stored in forward order. Repeat Task.
 *
 *   Solution:
 *   Initially went with an iterative solution, which is feasible, but recursion will
 *   work better and is better for me to practice anyway.
 *
 *   Recursively work through each list, adding values at each node, and passing the carry
 *   to the next recursive call. When both lists come back as null and the carry is empty
 *   the recursion is done and new list created.
 *
 *   MY SOLUTION IS STRAIGHT FROM THE BOOK. WHAT I WROTE BY HAND WAS SHIT CODE, ALTHOUGH STILL FUNCTIONAL (MOSTLY)
 */
public class problem5 {

    public LinkedListNode<Integer> addLists(LinkedListNode<Integer> num1, LinkedListNode<Integer> num2, int carry) {

        if (num1 == null && num2 == null && carry == 0) {
            //done with recursion, nothing more to add, return null and end method
            return null;
        }

        LinkedListNode<Integer> result = new LinkedListNode<>();
        int value = carry;

        if (num1 != null) {
            value += num1.getData();
        }
        if (num2 != null) {
            value += num2.getData();
        }

        result.setData(value % 10);  //second digit of number

        //Recursive call
        if (num1 != null || num2 != null) {
            LinkedListNode<Integer> more = addLists(num1 == null ? null : num1.getNext(),
                                                    num2 == null ? null : num2.getNext(),
                                                    value >= 10 ? 1 : 0);
            result.setNext(more);
        }
        return result;
    }
}
