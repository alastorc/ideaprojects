package chapter.two;

/**
 * Created by alastorc on 2/1/2016.
 *
 * Code below implements a basic singly linked list.
 * This is taken directly from CTCI Chapter 2 (Linked Lists)
 *
 */
class Node {
    Node next = null;
    int data;

    public Node(int d) {
        data = d;
    }

    void appendToTail(int d) {
        Node end = new Node(d);
        Node n = this;
        while   (n.next != null) {
            n = n.next;
        }
        n.next = end;
    }
}
