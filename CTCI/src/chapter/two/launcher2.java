package chapter.two;

/**
 * Created by alastorc on 2/1/2016.
 */
public class launcher2 {

    public static void main(String[] args) {
        /* PROBLEM #1 -------------------------------
        LinkedList<String> list = new LinkedList<String>();

        list.addFirst("first");
        list.addFirst("first");
        list.addFirst("second");
        list.addFirst("third");
        list.addFirst("third");
        list.addFirst("second");
        list.addFirst("first");
        list.addFirst("third");
        System.out.println("original list");
        list.printList();
        System.out.println();


        problem1 p1 = new problem1();
        //removing duplicates with hash table
        p1.deleteDupsHash(list.getFirstNode());

        System.out.println();
        System.out.println("list without duplicates: ");
        list.printList();
        ------------------------------------------- */

        /* PROBLEM #2 ----------------------------------
        LinkedList<String> list = new LinkedList<>();

        list.addFirst("first");
        list.addFirst("first");
        list.addFirst("second");
        list.addFirst("third");
        list.addFirst("third");
        list.addFirst("second");
        list.addFirst("first");
        list.addFirst("third");
        System.out.println("original list");
        list.printList();
        System.out.println();

        int k = 3;  //returns 3rd from last element in list

        //Integer c = 0;  //Integer class is immutable
        IntWrapper intObj = new IntWrapper(0);

        problem2 p2 = new problem2();
        LinkedListNode<String> r;
        r = p2.nthToLast(list.getFirstNode(), intObj, k);
        System.out.println("Value of " + k + " from last node is: " + r.getData());
        --------------------------------------------- */

        /* PROBLEM #3 -------------------------------
        LinkedList<String> list = new LinkedList<>();

        list.addFirst("first");
        list.addFirst("first");
        list.addFirst("second");
        list.addFirst("third");
        list.addFirst("third");
        list.addFirst("second");
        list.addFirst("first");
        list.addFirst("third");
        System.out.println("original list");
        list.printList();
        System.out.println();

        problem3 p3 = new problem3();
        //TEST
        int t = 0;
        System.out.println("Node at " + t + ": " + list.getNode(t).getData());

        p3.removeNodeSingle(list.getNode(t));
        System.out.println("list with removed node");
        list.printList();
        ----------------------------------------- */

        /* PROBLEM #4 -----------------------------
        LinkedList<Integer> l = new LinkedList<>();

        l.addFirst(8);
        l.addFirst(5);
        l.addFirst(1);
        l.addFirst(2);
        l.addFirst(18);
        l.addFirst(22);
        l.addFirst(10);
        l.addFirst(45);
        l.addFirst(32);
        l.addFirst(77);
        l.addFirst(14);
        l.addFirst(44);
        l.addFirst(9);
        l.addFirst(99);
        l.addFirst(61);
        l.addFirst(58);
        l.addFirst(13);
        System.out.println("original list");
        l.printList();
        System.out.println();

        problem4 p4 = new problem4();
        p4.partitionList(l, 10);

        System.out.println("partitioned list");
        l.printList();
        ---------------------------------------- */

        /* PROBLEM #5 ----------------------------

        LinkedListNode<Integer> n14 = new LinkedListNode<>(6, null);
        LinkedListNode<Integer> n13 = new LinkedListNode<>(8, n14);
        LinkedListNode<Integer> n12 = new LinkedListNode<>(7, n13);
        LinkedListNode<Integer> n11 = new LinkedListNode<>(3, n12);

        LinkedListNode<Integer> n23 = new LinkedListNode<>(2, null);
        LinkedListNode<Integer> n22 = new LinkedListNode<>(9, n23);
        LinkedListNode<Integer> n21 = new LinkedListNode<>(5, n22);

        int c = 0;

        problem5 p5 = new problem5();
        LinkedListNode<Integer> res = p5.addLists(n11, n21, c);
        res.printNodes();
        //LinkedList<Integer> r = new LinkedList<>();
        //r.insertList(p5.addLists(n11, n21, c));
        //r.printList();
        -------------------------------------------- */

        /* PROBLEM #6 -------------------------------

        //creating list with loop to find
        int lSize = 20;  //create a list with 20 nodes
        LinkedListNode<String> start = new LinkedListNode<>("0");
        LinkedListNode<String> node = start;
        for (int i = 1; i < lSize; i++) {
            node.createNext(Integer.toString(i));
            node = node.getNext();
        }
        System.out.println("original node order");
        start.printNodes();

        LinkedListNode<String> loopStart = new LinkedListNode<>();
        int set = 0;
        node = start;
        while (set < lSize) {
            if (set == 5) {  //node to loop to
                loopStart = node;
            }
            if (set == 19) {  //point end of the list to the loop start node
                node.setNext(loopStart);
            }
            node = node.getNext();
            set++;
        }
        System.out.println(loopStart.getData());
        //THIS WILL LOOP FOREVER AT THIS POINT, IF DONE CORRECTLY
        //start.printNodes();

        problem6 p6 = new problem6();
        LinkedListNode<String> r = p6.findLoop(start);
        System.out.println(r.getData());
        ---------------------------------------------- */

        /* PROBLEM #7 ------------------------------------ */
        LinkedListNode<Character> start = new LinkedListNode<>();
        LinkedListNode<Character> node = start;
        String hue = "backwards";
        System.out.println(hue.length());

        //setting up linked list with chars as data
        node.setData(hue.charAt(0));
        for (int i = 1; i < hue.length(); i++) {
            //System.out.println(hue.charAt(i));
            node.createNext(hue.charAt(i));  //creates a next node and sets the next of this one to it
            node = node.getNext();
        }
        start.printNodes();

        problem7 p7 = new problem7();
        boolean r = p7.isPalindrome(start);
        System.out.println("list is palindrome: " + r);
    }

}
