package chapter.two;

/**
 * Created by alastorc on 2/2/2016.
 */
public class IntWrapper {
    int i;

    public IntWrapper(int in) {
        i = in;
    }

    public void setInt(int in) {
        i = in;
    }

    public int getInt() {
        return i;
    }
}
