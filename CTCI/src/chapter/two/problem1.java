package chapter.two;

import java.util.Hashtable;

/**
 * Created by alastorc on 2/1/2016.
 */

public class problem1 {

    void deleteDupsHash(LinkedListNode<String> n) {
        Hashtable<String, Boolean> table = new Hashtable<>();
        LinkedListNode<String> previous = null;

        while (n.getData() != null) {
            System.out.println(n.getData());
            if (table.containsKey(n.getData())) {
                previous.setNext(n.getNext());  //deleting the duplicates
            } else {
                table.put(n.data, true);
                previous = n;
            }

            n = n.getNext();

        }
    }
}
