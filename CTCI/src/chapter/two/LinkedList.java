package chapter.two;

/**
 * Created by alastorc on 2/1/2016.
 */

class LinkedList<E> {

    //head cap, not a value in list
    LinkedListNode<E> head;
    //tail cap, not a value in list
    LinkedListNode<E> tail;

    // CONSTRUCTOR
    public LinkedList() {
        this.head = new LinkedListNode<E>();  //initialization
        this.tail = new LinkedListNode<E>();  //initialization
        head.setNext(tail);  //start of list, only 2 nodes
    }


    //simple method to return head of the list
    public LinkedListNode<E> getFirstNode() {
        return head.getNext();
    }

    //method to replicate calling a list node from an index value
    public LinkedListNode<E> getNode(int listInd) {
        int count = 0;
        LinkedListNode<E> n = this.getFirstNode();

        while (n.getData() != null) {
            if (count == listInd) {
                return n;
            } else {
                count++;
                n = n.getNext();
            }
        }
        //return the first node, or tail node if there are no others, by default
        System.out.println("Index value >  than list size or negative, returning first value by default");
        return this.getFirstNode();
    }

    //method to add node to beginning of the list
    public void addFirst(E e) {
        LinkedListNode<E> node = new LinkedListNode<>();
        node.setNext(head.getNext());
        node.data = e;
        head.setNext(node);
    }

    public void insertList(LinkedListNode<E> node) {
        head.setNext(node);
        /*
        while (node != null) {
            if (node.getNext() == null) {
                node.setNext(tail);
            }
        }
        */
    }

    //method to move any node in the list to the beginning of the list
    public void makeFirst(LinkedListNode<E> node) {
        node.setNext(head.getNext());
        head.setNext(node);
    }

    public void printList() {
        LinkedListNode<E> curr = head.getNext();
        while (curr.getNext() != null) {
            System.out.println(curr.getData());
            curr = curr.getNext();
        }
    }

}
