package chapter.two;

/**
 * Created by alastorc on 2/4/2016.
 *
 * Problem:
 *   Implement a function to check if a linked list is a palindrome.
 *
 *   Solution:
 *
 */
public class problem7 {

    public boolean isPalindrome(LinkedListNode<Character> start) {
        LinkedListNode<Character> node = new LinkedListNode<>();
        node.setNext(null); //last node in this list
        LinkedListNode<Character> temp = start;

        int len = 0;  //to store the length of the original list
        while (temp != null) {
            node.setData(temp.getData());  //copying from front to back

            //preventing first node from being null
            //--- ORDER OF THESE STATEMENTS IS IMPORTANT!!! ---
            if (temp.getNext() != null) {
                node = node.createPrev();
            }
            temp = temp.getNext();
            len++;
        }
        node.printNodes();

        System.out.println(Math.ceil(len/2.0));
        temp = start;  //back to beginning
        //note that I only need to check the first half of each list to the other
        for (int i = 0; i < Math.ceil(len/2.0); i++) {
            System.out.println("iteration #: " + i);
            if (temp.getData() != node.getData()) {
                return false;
            } else {
                temp = temp.getNext();
                node = node.getNext();
            }
        }

        return true;
    }
}
