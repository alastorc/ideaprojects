package chapter.two;

/**
 * Created by alastorc on 2/2/2016.
 *
 * Problem:
 *   Implement an algorithm to delete a node in the middle of a
 *   singly linked list, given only access to that node.
 *
 * Solution:
 *   If node desired to be deleted is the last node, simply clear out all of info within that node.
 *   Otherwise, set the data of node to be deleted to the data of successive node, and do the same with
 *   the list pointer.  This technically removes the successive node from the list, and keeps the one
 *   passed in for deletion, but since the given node now holds all the same info it is essentially removed.
 */
public class problem3 {

    public void removeNodeSingle(LinkedListNode<Integer> node) {  //Integer will need to be changed given list involved
        if (node.getNext().getData() == null) {
            //clearing out all information, making this the tail of the list
            node.setData(null);
            node.setNext(null);
        } else {
            //copying over data
            node.setData(node.getNext().getData());
            //copying over list pointer and removing successive node
            node.setNext(node.getNext().getNext());
        }
    }

}
