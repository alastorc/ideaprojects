package chapter.two;

/**
 * Created by alastorc on 2/1/2016.
 */
public class LinkedListNode<E> {

    //class variables
    E data;
    private LinkedListNode<E> next;


    //CONSTRUCTORS
    public LinkedListNode(){}

    public LinkedListNode(E in) {
        data = in;
    }

    public LinkedListNode(LinkedListNode<E> node) {
        next = node;
    }

    public LinkedListNode(E in, LinkedListNode<E> node) {
        data = in;
        next = node;
    }


    //GETTERS AND SETTERS

    //create new node, expecting data for that node to be passed in
    //this one returns the
     public void createNext() {
         LinkedListNode<E> newNode = new LinkedListNode<>();
         this.setNext(newNode);
    }

    //overloading, add data automatically in this one
    public void createNext(E d) {
        LinkedListNode<E> newNode = new LinkedListNode<>(d);
        this.setNext(newNode);
    }

    public LinkedListNode<E> createPrev() {
        LinkedListNode<E> newNode = new LinkedListNode<>();
        newNode.setNext(this);
        return newNode;
    }

    public LinkedListNode<E> getNext() {
        return next;
    }

    public void setNext(LinkedListNode<E> next) {
        this.next = next;
    }

    public E getData() {
        return data;
    }

    public void setData(E in) {
        data = in;
    }

    public void printNodes() {
        LinkedListNode<E> node = this;
        while (node != null) {
            System.out.print(node.getData() + " ");
            node = node.getNext();
        }
        System.out.println();
    }
}
