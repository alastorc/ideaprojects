package chapter.two;

/**
 * Created by alastorc on 2/2/2016.
 *
 * Problem:
 *   Write code to partition a linked list around a value x,
 *   such that all currs less than x come before all currs
 *   greater than or equal to x.
 *
 * Solution:
 *
 */
public class problem4 {

    public void partitionList(LinkedList<Integer> list, int x) {
        LinkedListNode<Integer> curr = list.getFirstNode();
        //LinkedListNode<Integer> next;
        problem3 p3 = new problem3();  //using answer to previous problem

        while (curr.getData() != null) {  //loop thru list
            if (curr.getData() < x) {  //need to move values < x to beginning half of list
                int temp = curr.getData();  //save the data stored in current curr
                //next = curr.getNext();
                //next node becomes this node, no need to change curr after this deletion
                p3.removeNodeSingle(curr);  //clear current curr from the list
                list.addFirst(temp);  //add a new node onto the beginning of the list w/ same info as current node
            } else {  //do nothing to node, move along in list
                curr = curr.getNext();
            }

        }
    }
}
