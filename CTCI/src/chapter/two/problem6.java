package chapter.two;

/**
 * Created by alastorc on 2/4/2016.
 *
 * Problem:
 *   Given a circular linked list, implement an algorithm which returns
 *   the node at the beginning of the loop.
 *
 * Solution:
 *   Check the book and my notes for the detailed algorithm for this.
 *   Summary:  Have a slow pointer that moves one node (n) every step.
 *   Have another pointer that moves 2 nodes (2n) every step.
 *   Loop, moving them at this rate, until they collide.  They will always, if there is a loop.
 *   Move one of the pointers back to the beginning of the list.
 *   Move them at the same rate, (n), until they collide again, at which point the loop start has been found.
 *
 * Algorithm Summary:
 *   Key is knowing that we are coming into the loop from a normal series of nodes, thus indicating that
 *   there must be a start to the loop.  Knowing that the pointers move at set speeds, with speed of change being
 *   one node per step.  Start of list is k nodes away from the beginning of the loop.  Once the slow pointer
 *   has reached the start of the loop, the fast pointer has moved 2k-k nodes in the loop.  They will meet k
 *   nodes away from the start of the loop.  Move one pointer back to beginning to go that k nodes to the start
 *   of the loop.  Since there is no way to store value of k, must be done this way.
 */
public class problem6 {

    /* ----------------------------------
        CHECK MY NOTES AND THE BOOK FOR THE MATH TO THIS SOLUTION
       --------------------------------- */
    public LinkedListNode<String> findLoop(LinkedListNode<String> start) {
        LinkedListNode<String> slowPtr = start;
        LinkedListNode<String> fastPtr = start;

        //go until loop is found or list end is reached
        while (fastPtr != null && fastPtr.getNext() != null) {
            slowPtr = slowPtr.getNext();  //move one node at a time
            fastPtr = fastPtr.getNext().getNext();  //move 2 nodes at a time

            if (slowPtr == fastPtr) {  //collision, and TF loop, detected
                slowPtr = start;  //move one pointer to start of the list
                while (fastPtr != slowPtr) {  //finding start of loop
                    //move both pointers along one step at a time until another collision
                    fastPtr = fastPtr.getNext();
                    slowPtr = slowPtr.getNext();
                }
                System.out.print("loop start node: ");
                return slowPtr;
            }
        }
        System.out.println("no loop detected");
        return start;
    }
}
