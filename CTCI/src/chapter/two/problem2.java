package chapter.two;


/**
 * Created by alastorc on 2/2/2016.
 *
 * Question:
 * Implement an algorithm to find the kth to last element of a singly linked list
 *
 * Solution:
 *   Recursive:
 *     Recurse through to the end.  Start counter.  Compare counter to the k value
 *     (# of steps back from end).  If the count <= k return self, else return the same node
 *     that was returned from the previous call.
 *
 *     Key here is that the counter is an Integer object, as this will be passed
 *     by reference and therefore be viewable by the node within each recursive call
 *
 *   Iterative:
 */
public class problem2 {

    public LinkedListNode<String> nthToLast(LinkedListNode<String> node, IntWrapper count, int k) {

        if (node.getNext().getData() == null) {  //last node in list
            count.setInt(1); //starting counter
            System.out.println("last node in list. value: " + node.getData() + ", counter is " + count.getInt());
            return node;  //returning this node, last in the list
        } else {
            LinkedListNode<String> newNode;
            newNode = nthToLast(node.getNext(), count, k);  //recursive call

            count.setInt(count.getInt() + 1);  //incrementing counter
            System.out.println("node with value: " + node.getData() + ", counter is " + count.getInt());

            /* Key bit here. If count has incremented up to the value of k, return
             * the node that was passed into this call.  Otherwise, return the node
             * that this call was originally created for.
             */
            if (count.getInt() <= k) {
                return node;
            } else {
                return newNode;  //solution node
            }
        }
    }

}
