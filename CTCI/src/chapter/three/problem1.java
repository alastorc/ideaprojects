package chapter.three;

/**
 * Created by alastorc on 2/5/2016.
 *
 * Problem:
 *   Describe how you could use a single array to implement three stacks.
 *
 * Solution:
 *
 */
public class problem1 {

    final int arrSize = 100;
    final int numOfStacks = 3;

    //save location of top for each stack
    int[] tops = {-1, -1, -1};
    int[] sizes = {0, 0, 0};

    //stacks are full of integers
    int[] stackArr = new int[arrSize];

    //custom class for stack functions on array
    //StackManager<Integer> tools = new StackManager<>();

    //tools.push();

}
