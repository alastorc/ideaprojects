package chapter.three;

/**
 * Created by alastor on 18-Apr-16.
 */
public class problem2 extends StackManager {

    /*
     * Keep track of the minimum value in each stack in O(1) time
     * Needs another stack. Keeps the access time constant, takes more space than
     * other options.
     *
     * ADDING MINS:
     * Every time a min value enters the stack, aka the new value is compared
     * to the top of the min stack and is <, then add it to the top of the min stack.
     *
     * REMOVING MINS:
     * How to know when to pop things off the stack when the current min gets
     * popped off the main stack? Every pop, check to see if it matches the top
     * value in mine array. IF == TOP, then REMOVE. Must needs store min values
     * in that stack if they <=, not just <. Solves issue of premature removal.
     *
     * Whenever you want to know the min of the stack, just peek at the top of
     * the min stack for that stack. It will contain the min for the main stack
     * at any given time.
     *
     */
}
