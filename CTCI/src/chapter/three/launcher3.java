package chapter.three;

import java.util.Scanner;

/**
 * Created by alastorc on 2/5/2016.
 */
public class launcher3 {

    public static void main(String[] args) {

        /* PROBLEM #1 ---------------------- */
        StackManager p1 = new StackManager();
        p1.initStack();

        System.out.println("PUSHING");

        /* ---- first stack ------ */

        int M = 3;
        for (int i = 0; i < M; i++) {
            try {
                p1.push(0, i*10+1);
            } catch (Exception e) {
                System.out.println("Err: " + e);
            }
            System.out.println("-----------------------------------------------");
        }
        System.out.println();


        /* -----second stack ----- */

        int N = 4;
        for (int i = 0; i < N; i++) {
            try {
                p1.push(1, i*10+2);
            } catch (Exception e) {
                System.out.println("Err: " + e);
            }
            System.out.println("-----------------------------------------------");
        }
        System.out.println();


        /* ------ third stack ----- */

        int Z = 5;
        for (int i = 0; i < Z; i++) {
            try {
                p1.push(2, i*10+3);
            } catch (Exception e) {
                System.out.println("Err: " + e);
            }
            System.out.println("-----------------------------------------------");
        }

        System.out.println();
        System.out.println();


        /* ----- SOME OTHER TESTS ----- */
        System.out.println("Array of stack values is full: " + p1.isFull());
        System.out.println();

        int stkOpt;
        //USER INPUT BELOW
        while (true) {
            Scanner reader = new Scanner(System.in);
            System.out.println("Please enter choose an option: ");
            System.out.println("0: PUSH");
            System.out.println("1: POP");
            System.out.println("2: PEEK");
            System.out.println("3: STACK INFO");
            System.out.println("4: EXIT");
            System.out.println("---------------");
            int opt = reader.nextInt();



            switch (opt) {
                case 0:
                    System.out.println("Please choose a stack to perform function on: ");
                    stkOpt = reader.nextInt();
                    System.out.println("Enter integer value: ");
                    int val = reader.nextInt();
                    pusher(p1, stkOpt, val);
                    break;
                case 1:
                    System.out.println("Please choose a stack to perform function on: ");
                    stkOpt = reader.nextInt();
                    popper(p1, stkOpt);
                    break;
                case 2:
                    System.out.println("Please choose a stack to perform function on: ");
                    stkOpt = reader.nextInt();
                    peeker(p1, stkOpt);
                    break;
                case 3:
                    infoDisplay(p1);
                    break;
                case 4:
                    System.exit(0);
                //default: continue;
            }

        }



/*
        System.out.println("POPPING");

        try {
            System.out.println("0: returned: " + p1.pop(0));
        } catch (Exception e) {
            System.out.println("Err : " + e);
        }
        System.out.println("---------------------");

        try {
            System.out.println("1: returned: " + p1.pop(1));
        } catch (Exception e) {
            System.out.println("Err : " + e);
        }
        System.out.println("---------------------");

        try {
            System.out.println("2: returned: " + p1.pop(2));
        } catch (Exception e) {
            System.out.println("Err : " + e);
        }
        System.out.println("---------------------");
*/

    }

    public static void pusher(StackManager obj, int stk, int val) {
        try {
            obj.push(stk, val);
        } catch (Exception e) {
            System.out.println("Err: " + e);
        }
        System.out.println("-----------------------------------------------");
        System.out.println();
    }

    public static void popper(StackManager obj, int stk) {
        try {
            obj.pop(stk);
        } catch (Exception e) {
            System.out.println("Err: " + e);
        }
        System.out.println("-----------------------------------------------");
        System.out.println();
    }

    public static void peeker(StackManager obj, int stk) {
        try {
            System.out.println(obj.peek(stk));
        } catch (Exception e) {
            System.out.println("Err: " + e);
        }
        System.out.println("-----------------------------------------------");
        System.out.println();
    }

    public static void infoDisplay(StackManager obj) {
        obj.printStackInfo();
    }
}
