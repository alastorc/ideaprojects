package chapter.three;

/**
 * Created by alastorc on 2/29/2016.
 *
 * Implementation of a data structure needed for questions in Chapter 3 of Cracking the Coding Interview
 *
 */

/**
 * Things this will do:
 *
 * push
 * pop
 * peek
 * shift array (for dynamically sized stacks within in array)
 *
 * Designed to handle an array through the tools of a stack, providing LIFO access
 * Specifically, this is for an array that has multiple dynamically-sized stacks working within it.
 */
public class StackManager {

    /* ---------- STARTER VARIABLES --------- */


    int arraySize = 15;
    Integer stackVals[] = new Integer[arraySize];
    int numOfStacks = 3;
    StackInfo[] allStacks = new StackInfo[numOfStacks];


    /*---------- START, MANDATORY ---------- */


    public void initStack() {

        for (int i = 0; i < numOfStacks; i++) {
            //initialize objects
            allStacks[i] = new StackInfo();
            System.out.println(arraySize / numOfStacks * i);

            allStacks[i].currSize = 0;

            allStacks[i].top = -1;  //this will be set appropriately at the first push, then incremented or decremented appropriately

            /*  THE BOTTOM IS AN IMPORTANT PLACEHOLDER FOR EACH STACK
                -> even if a stack is empty and the stack below it needs space,
                   a shift must be performed to move the bottom as it designates the starting point for a stack
             */
            allStacks[i].bottom = arraySize / numOfStacks * i;  //will only change during a shift

            System.out.println("-----------------------");
            System.out.println();
        }
        printStackInfo();
        System.out.println();
    }  //END INITSTACK


    /* --------------- PUSH, SHIFT, POP, PEEK --------------- */


     /*
     * 1. check if value in index of array succeeding the current top is empty
     * 2. if yes, then stack is full. attempt a shift. if array is totally full, return error. else, shift and cont.
     * 3. after empty space is found or a shift is made, then move the top of stack up to succeeding index
     *    and save the value into this array location.
     */
    public void push(int id, int data) throws Exception {
        System.out.println("Attempting push into stack " + id);

        //check if the current stack has run out of space
        //NOTE: if allStacks[id].top == -1 then the bottom placeholder is there and AT LEAST ONE value can be pushed in all scenarios
        if (allStacks[id].top != -1 && ( (allStacks[id].top + 1) % arraySize ) == allStacks[(id + 1) % numOfStacks].bottom ) {
            //TEST PRINT
            System.out.println("top + 1 % arraySize: " + (allStacks[id].top + 1) % arraySize + " -> bottom of next: " + allStacks[(id + 1) % numOfStacks].bottom);

            //check if entire array is out of space. if not, attempt shift
            if (isFull()) {
                throw new Exception("Entire array is out of space");
            } else {  //if this point is reached, it is guaranteed that a shift is possible
                System.out.println("attempting a shift");
                shift(id);
            }

        }

        //THIS CONDITIONAL MAY BE BULKY
        //increasing/setting top variable if is has not been yet
        if (allStacks[id].top == -1) {
            allStacks[id].top = allStacks[id].bottom;  //because bottom keeps up with shifts!!!
        } else {
            allStacks[id].top = (allStacks[id].top + 1) % arraySize;
        }

        stackVals[allStacks[id].top] = data;
        allStacks[id].currSize++;

        printStackInfo();
        /*
        System.out.println("Top in " + id + " is: " + stackVals[allStacks[id].top]);
        System.out.println(id + ": current size: " + allStacks[id].currSize);
        System.out.println(id + ": top is @: " + allStacks[id].top);
        System.out.println(id + ": bottom is @: " + allStacks[id].bottom);
        */
    }  //END PUSH


    /*
     * 1. start iteration at the first space past the top of the stack passed in
     *      continue to loop through each space until an empty (null) is found
     * 2. once space is found, shift all elements one to the right of its current space
     *          a. use modulo arithmetic
     */
    public void shift(int id) {
        int stk = id;
        int i;

        for (i = 0; i < numOfStacks - 1; i++) {  //check each stack for space available, until space is found
            //incrementing
            stk = (stk + 1) % numOfStacks;

            System.out.println("searching for space in stack: " + stk);

            //check if stack is empty
            if (allStacks[stk].top == -1) {  //assuming that there is space right now...
                System.out.println("stack " + stk + " has nothing. Shifting");
                //nothing in stack, assuming there is space, attempting shift
                shiftRange(allStacks[id].top, allStacks[stk].bottom);
                //printStackInfo();

                break;

            //check if stack is full. if this is not checked and the next stack has nothing in it
            //then the last check for an empty adjacent slot will be true, but could be in another stack
            } else if ( (allStacks[stk].top + 1) % arraySize == allStacks[(stk + 1) % numOfStacks].bottom) {
                System.out.println("stack " + stk + " is full as well, moving to next stack");
                //another full stack, so skip

                continue;

            //lastly, check for the empty adjacent slot in array
            } else if (stackVals[allStacks[stk].top + 1] == null) {

                System.out.println("Found empty spot in stack " + stk + ". Shifting");
                shiftRange(allStacks[id].top, allStacks[stk].top);
                //printStackInfo();

                break;
            }  //else try in the next stack
        }
        System.out.println("i = " + i);
        System.out.println("stack @ the end of shifting is " + stk);

        //have to update the top and bottom of all the stacks involved in shift
        //start with the stk variable unchanged, and go backwards to the requesting stack
        for (int j = i; j >= 0; j--) {

            //HAVE TO UPDATE TOP AND BOTTOM FOR ALL STACKS BEING SHIFTED
            if (allStacks[stk].top != -1) {
                System.out.println("Stack " + stk + " new top: " + (allStacks[stk].top + 1) % arraySize);
                allStacks[stk].top = (allStacks[stk].top + 1) % arraySize;
            }
            System.out.println("Stack " + stk + " new bottom: " + (allStacks[stk].bottom + 1) % arraySize);
            allStacks[stk].bottom = (allStacks[stk].bottom + 1) % arraySize;

            stk--;
            if (stk < 0) {
                stk = numOfStacks - 1;
            }
        }

        System.out.println("space has been made in the array");
        System.out.println("POST-SHIFT but PRE-PUSH INFO");
        printStackInfo();
    }  //END SHIFT


    /*
     * 1. find value at the top of stack, save it for return
     * 2. clear the value at top of stack
     * 3. change the top of stack pointer to address of value preceding the current top
     * 3. return the top of the stack
     */
    public int pop(int id) throws Exception {
        if (allStacks[id].currSize <= 0) {
            throw new Exception("nothing in stack");
        } else {
            int r = stackVals[allStacks[id].top];
            allStacks[id].currSize--;
            allStacks[id].top--;

            //CLEAR POPPED VALUE!!!

            //have to reset top give how pushing/shifting works
            if (allStacks[id].currSize <= 0) {
                allStacks[id].top = -1;
            }
            printStackInfo();
            return r;
        }
    }  //END POP


    /*
     * 1. find and return the value at the top of the stack
     * 2. if a top does not exist, catch exception and return an error
     */
    public int peek(int id) {
        if (allStacks[id].currSize == 0) {
            System.out.println("stack is empty");
            return 0;  //TODO: catch as an error
        } else {
            return stackVals[allStacks[id].top];
        }
    }  //END PEEK


    /* ------- CONVENIENCE METHODS -------- */

    //check to see if the array of stacks is completely filled up
    public boolean isFull() {
        int totalSize = 0;
        for (int i = 0; i < numOfStacks; i++) {
            totalSize += allStacks[i].currSize;
        }
        System.out.println("# of values in array currently: " + totalSize);
        if (totalSize >= arraySize) {  //double check this comparison
            return true;
        } else {
            return false;
        }
    } //END ISFULL


    //right to left
    //shift each value to the right one
    //start with i2, go to i1 + 1
    public void shiftRange(int i1, int i2) {
        int ind = i2;
        Integer temp;
        do {
            temp = stackVals[ind];
            stackVals[(ind + 1) % arraySize] = temp;

            System.out.println("Place in array: " + ind);
            System.out.println("Value being moved: " + temp);

            ind--;
            if (ind < 0) {
                ind = arraySize - 1;
            }

        } while (ind != i1);
    }  //END SHIFTRANGE


    //FOR TESTING
    public void printStackInfo () {
        System.out.println();
        System.out.println("-------------------------------");
        System.out.println("INFORMATION FOR ALL STACKS!!!");
        for (int i = 0; i < numOfStacks; i++) {
            System.out.println("Stack #: " + i);
            System.out.println("    Top: " + allStacks[i].top);
            System.out.println("    Bottom: " + allStacks[i].bottom);
            System.out.println("    Size: " + allStacks[i].currSize);
            if (allStacks[i].top > -1) {
                System.out.println("    Top Value: " + stackVals[allStacks[i].top]);
            }
            System.out.println("-------------------------------");
        }
        System.out.println();
    }
}
